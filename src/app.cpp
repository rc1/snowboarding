#include "app.h"

#define wLABEL_ELAPSED "_elapsed app time"
#define wLABEL_VIDEO_TIME "_video time"
#define wTOGGLE_3D "View 3D"
#define wBUTTON_FULLSCREEN "Fullscreen"
#define wRGB_BLUE 66, 145, 195
#define wRGB_GREEN 116, 168, 138
#define wRGB_PINK 232, 137, 179

//--------------------------------------------------------------
void app::setup(){
    
    ofSetLogLevel(OF_LOG_VERBOSE); 
    
    // video
    boarderVideo.loadMovie("tor/tor.mov");
    boarderVideo.setVolume(0);
    boarderAspectRatio = float(boarderVideo.width) / float(boarderVideo.height);
    ofLogVerbose() << "aspect ratio:" << boarderAspectRatio;
    boarderVideo.play();
    
    // gui
    gui = new ofxUICanvas(ofGetWidth()-320,0,320,800);
    gui->addWidgetDown(new ofxUILabel("APP INFO", OFX_UI_FONT_LARGE));
    gui->addWidgetDown(new ofxUIFPS(OFX_UI_FONT_MEDIUM)); 
    gui->addWidgetDown(new ofxUILabel(wLABEL_ELAPSED, "0:00:00", OFX_UI_FONT_MEDIUM));
    gui->addWidgetDown(new ofxUILabel(wLABEL_VIDEO_TIME, "0:00:00", OFX_UI_FONT_MEDIUM));
    gui->addWidgetDown(new ofxUILabelToggle(false, wTOGGLE_3D, OFX_UI_FONT_MEDIUM));
    gui->addWidgetDown(new ofxUILabelButton(false, wBUTTON_FULLSCREEN, OFX_UI_FONT_MEDIUM));
    ofAddListener(gui->newGUIEvent,this,&app::guiEvent);
    
    // rider data
    datapoints = parseRiderData("tor/RawLog.xml");
    latLongTL = ofPoint(10, 10);
    latLongBR = ofPoint(ofGetWidth()-20, ofGetHeight()-277);
    
    // heartrate and speed displays
    heartrateSize = ofVec4f(400,10,100,200);
    speedSize = ofVec4f(600,10,100,200);
    altitudeSize = ofVec4f(800,10,100,200);
    
}

//--------------------------------------------------------------
void app::update(){
    boarderVideo.idleMovie();
}

//--------------------------------------------------------------
void app::draw(){
    
    ///
    // View Underlay
    boarderVideo.draw(0,0,800*boarderAspectRatio,800);
    ofPushStyle();
    ofEnableAlphaBlending();
    ofSetColor(0, 0, 0, 200);
    ofRect(0, 0, ofGetWidth(),ofGetHeight());
    ofDisableAlphaBlending();
    ofPopStyle();
    
    ///
    // Update GUI
    ofxUILabel *elapsed = (ofxUILabel*) gui->getWidget(wLABEL_ELAPSED);
    elapsed->setLabel("APP TIME: "+ofToString(ofGetElapsedTimeMillis()));
    ofxUILabel *videoTime = (ofxUILabel*) gui->getWidget(wLABEL_VIDEO_TIME);
    videoTime->setLabel("VIDEO TIME: "+ofToString(boarderVideo.getPosition()*100000));
    
    ///
    // Video Overlay
    ofPushStyle();
    ofSetColor(wRGB_GREEN);
    ofRect(8,ofGetHeight()-212,(200*boarderAspectRatio)+4,204);
    ofPopStyle();
    boarderVideo.draw(10,ofGetHeight()-210,200*boarderAspectRatio,200);
    
    ///
    // Draw max lat an lon points
    ofPushStyle();      
    ofNoFill();
    ofSetColor(wRGB_PINK);
    ofRect(latLongTL, latLongBR.x, latLongBR.y);
    ofPopStyle();
    
    //camera.begin();
    
    float prevx = NULL;
    float prevy = NULL;
    float prevz = NULL;
    
    for(std::vector<GPSDataPoint>::iterator gps = datapoints.gps.begin(); gps != datapoints.gps.end(); ++gps) {
        // draw a square for each gps point
        float y = ofMap(gps->longitude, datapoints.lowestLongitude, datapoints.highestLongitude, latLongTL.y, latLongBR.y);
        float x = ofMap(gps->latitude, datapoints.lowestLatitude, datapoints.highestLatitude, latLongTL.x, latLongBR.x);
        float z = ofMap(gps->altitude, datapoints.lowestAltitude, datapoints.highestAltitude, -300, 0);
        ofxUIToggle *toggle3d = (ofxUIToggle*)gui->getWidget(wTOGGLE_3D);
        if (toggle3d->getValue()) {
            ofRect(x-2, y-2, z, 4, 4);
            if (prevx && prevy) {
                ofLine(x, y, z, prevx, prevy, prevz);
            }
        } else {
            ofRect(x-2, y-2, 4, 4);
            if (prevx && prevy) {
                ofLine(x, y, prevx, prevy);
            }
        }
        prevy = y;
        prevx = x;
        prevz = z;
    }
    
    GPSDataPoint currentGPSPoint = datapoints.gps[0];
    // cout << (boarderVideo.getPosition()*100000) << " " << currentGPSPoint.tstamp - datapoints.lowestTimestamp << endl;
    for(std::vector<GPSDataPoint>::iterator gps = datapoints.gps.begin(); gps != datapoints.gps.end(); ++gps) {
        if (gps->tstamp > currentGPSPoint.tstamp && gps->tstamp - datapoints.lowestTimestamp <= boarderVideo.getPosition()*100) {
            
            //cout << boarderVideo.getPosition()*100 << " " << currentGPSPoint.tstamp - datapoints.lowestTimestamp << endl;
            currentGPSPoint = *gps;
        }
    }
    
    ofPushStyle();
    ofSetColor(wRGB_BLUE);
    ofSetCircleResolution(25);
    float y = ofMap(currentGPSPoint.longitude, datapoints.lowestLongitude, datapoints.highestLongitude, latLongTL.y, latLongBR.y);
    float x = ofMap(currentGPSPoint.latitude, datapoints.lowestLatitude, datapoints.highestLatitude, latLongTL.x, latLongBR.x);
    float z = ofMap(currentGPSPoint.altitude, datapoints.lowestAltitude, datapoints.highestAltitude, -300, 0);
    ofxUIToggle *toggle3d = (ofxUIToggle*)gui->getWidget(wTOGGLE_3D);
    if (toggle3d->getValue()) {
        ofCircle(x-2, y-2, z, 4);
    } else {
        ofCircle(x-2, y-2, 4);
    }
    ofPopStyle();
    
    //camera.end();

    ///
    // Draw heart rate
    ofPushStyle();
    ofSetColor(wRGB_PINK);
    HeartRateDataPoint currentHeartrate = datapoints.heartrate[0];
    for(std::vector<HeartRateDataPoint>::iterator heart = datapoints.heartrate.begin(); heart != datapoints.heartrate.end(); ++heart) {
        if (heart->tstamp > currentGPSPoint.tstamp && heart->tstamp - datapoints.lowestTimestamp <= boarderVideo.getPosition()*100) {
            currentHeartrate = *heart;
        }
    }
    float heartheight = ofMap(currentHeartrate.averageOverLastBeats, datapoints.lowestHeartrate, datapoints.highestHeartrate, 0, heartrateSize.w);
    ofRect(heartrateSize.x, ofGetHeight()-heartheight-heartrateSize.y, heartrateSize.z, heartheight);
    ofSetColor(255, 255, 255);
    ofDrawBitmapString("heatrate: "+ofToString(currentHeartrate.averageOverLastBeats), heartrateSize.x, ofGetHeight()-heartrateSize.y);
    ofPopStyle();
    
    ///
    // Draw speed 
    ofPushStyle();
    ofSetColor(wRGB_BLUE);
    float speedheight = ofMap(currentGPSPoint.ground_speed, datapoints.lowestSpeed, datapoints.highestSpeed, 0, speedSize.w);
    ofRect(speedSize.x, ofGetHeight()-speedheight-speedSize.y, speedSize.z, speedheight);
    ofSetColor(255, 255, 255);
    ofDrawBitmapString("speed: "+ ofToString(currentGPSPoint.ground_speed), speedSize.x, ofGetHeight()-speedSize.y);
    ofPopStyle();
    
    ///
    // Draw altitude 
    ofPushStyle();
    ofSetColor(wRGB_GREEN);
    float altitudeheight = ofMap(currentGPSPoint.altitude, datapoints.lowestAltitude, datapoints.highestAltitude, 0, altitudeSize.w);
    ofRect(altitudeSize.x, ofGetHeight()-altitudeheight-altitudeSize.y, altitudeSize.z, altitudeheight);
    ofSetColor(255, 255, 255);
    ofDrawBitmapString("altitude:" + ofToString(currentGPSPoint.altitude), altitudeSize.x, ofGetHeight()-altitudeSize.y);
    ofPopStyle();
    
    ///
    // W
    ofPushStyle();
    ofSetColor(wRGB_BLUE);
    ofSetCircleResolution(25);
    ofCircle(ofGetWidth()-25.5f, ofGetHeight()-25.0f, 10.0f);
    ofPopStyle();
}

// MISC DRAWING
//--------------------------------------------------------------
void app::drawBorderPath()
{
    
}

// GUI
//--------------------------------------------------------------
void app::exit()
{
	gui->saveSettings("GUI/guiSettings.xml");     
    delete gui; 
}

void app::guiEvent(ofxUIEventArgs &e)
{
	string name = e.widget->getName(); 
	int kind = e.widget->getKind(); 
    
    if (name == wBUTTON_FULLSCREEN) {
        ofToggleFullscreen();
    }
    
}

// DATA POINTS
//--------------------------------------------------------------

DataPoints app::parseRiderData(string filename) {
    int numTags = 0;
    DataPoints parsedDatapoints;
    
    // set some inital values
    parsedDatapoints.lowestTimestamp = std::numeric_limits<double>::max();
    parsedDatapoints.lowestAltitude = std::numeric_limits<double>::max();
    parsedDatapoints.lowestLatitude = std::numeric_limits<double>::max();
    parsedDatapoints.lowestLongitude = std::numeric_limits<double>::max();
    parsedDatapoints.lowestHeartrate = std::numeric_limits<double>::max();
    parsedDatapoints.lowestSpeed = std::numeric_limits<double>::max();
    parsedDatapoints.highestTimestamp = std::numeric_limits<double>::min();
    parsedDatapoints.highestAltitude = std::numeric_limits<double>::min();
    parsedDatapoints.highestLatitude = std::numeric_limits<double>::min();
    parsedDatapoints.highestLongitude = std::numeric_limits<double>::min();
    parsedDatapoints.highestHeartrate = std::numeric_limits<double>::min();
    parsedDatapoints.highestSpeed = std::numeric_limits<double>::min();
    
    //ofLogVerbose() << std::numeric_limits<double>::min();;
    //ofLogVerbose("Data will load with start time of "+ofToString(parsedDatapoints.lowestTimestamp)+" and end time of "+ofToString(parsedDatapoints.highestTimestamp));
    
    
    // load the xml file, and set the root node
    ofxXmlSettings *riderData = new ofxXmlSettings(filename);
    riderData->pushTag("N8SensorsLog");
    
    // parse the gps
    numTags = riderData->getNumTags("gps_data");
    for (int i=0; i<numTags; i++) {
        GPSDataPoint point;
        point.tstamp = riderData->getAttribute("gps_data", "tstamp", 0.0, i);
        point.longitude = riderData->getAttribute("gps_data", "longitude", 0.0, i);
        point.latitude = riderData->getAttribute("gps_data", "latitude", 0.0, i);
        point.altitude = riderData->getAttribute("gps_data", "altitude", 0.0, i);
        point.ground_speed = riderData->getAttribute("gps_data", "ground_speed", 0.0, i);
        // set the lowest & highest Altitude
        if(point.altitude<parsedDatapoints.lowestAltitude) {
            parsedDatapoints.lowestAltitude = point.altitude;
        }
        if(point.altitude>parsedDatapoints.highestAltitude) {
            parsedDatapoints.highestAltitude = point.altitude;
        }
        // set the lowest & highest Latitude
        if(point.latitude<parsedDatapoints.lowestLatitude) {
            parsedDatapoints.lowestLatitude = point.latitude;
        }
        if(point.latitude>parsedDatapoints.highestLatitude) {
            parsedDatapoints.highestLatitude = point.latitude;
        }
        // set the lowest & highest Longitude
        if(point.longitude<parsedDatapoints.lowestLongitude) {
            parsedDatapoints.lowestLongitude = point.longitude;
        }
        if(point.longitude>parsedDatapoints.highestLongitude) {
            parsedDatapoints.highestLongitude = point.longitude;
        }
        // set the lowest & highest endtime
        if(point.tstamp<parsedDatapoints.lowestTimestamp) {
            parsedDatapoints.lowestTimestamp = point.tstamp;
        }
        if(point.tstamp>parsedDatapoints.highestTimestamp) {
            parsedDatapoints.highestTimestamp = point.tstamp;
        }
        // set the lowest  & highest speed
        if(point.ground_speed<parsedDatapoints.lowestSpeed) {
            parsedDatapoints.lowestSpeed = point.ground_speed;
        }
        if(point.ground_speed>parsedDatapoints.highestSpeed) {
            parsedDatapoints.highestSpeed = point.ground_speed;
        }

        parsedDatapoints.gps.push_back(point);
    }
    
    // parse the heartrate
    numTags = riderData->getNumTags("heart_data");
    for (int i=0; i<numTags; i++) {
        HeartRateDataPoint point;
        point.tstamp = riderData->getAttribute("heart_data", "tstamp", 0.0, i);
        point.averageOver32Beats = riderData->getAttribute("heart_data", "last32", 0.0, i);
        point.averageOver16Beats = riderData->getAttribute("heart_data", "last16", 0.0, i);
        point.averageOverLastBeats = riderData->getAttribute("heart_data", "lastBeat", 0.0, i);
        // set the lowest startime & highest endtime
        if(point.tstamp<parsedDatapoints.lowestTimestamp) {
            parsedDatapoints.lowestTimestamp = point.tstamp;
        }
        if(point.tstamp>parsedDatapoints.highestTimestamp) {
            parsedDatapoints.highestTimestamp = point.tstamp;
        }
        // set the lowest & highest heartrate
        if(point.averageOverLastBeats<parsedDatapoints.lowestHeartrate) {
            parsedDatapoints.lowestHeartrate = point.averageOverLastBeats;
        }
        if(point.averageOverLastBeats>parsedDatapoints.highestHeartrate) {
            parsedDatapoints.highestHeartrate = point.averageOverLastBeats;
        }
        parsedDatapoints.heartrate.push_back(point);
    }

    // parse the accelerometer
    numTags = riderData->getNumTags("acc_data");
    for (int i=0; i<numTags; i++) {
        AccelerometerDataPoint point;
        point.tstamp = riderData->getAttribute("acc_data", "tstamp", 0.0, i);
        point.x = riderData->getAttribute("acc_data", "x", 0.0, i);
        point.y = riderData->getAttribute("acc_data", "y", 0.0, i);
        point.z = riderData->getAttribute("acc_data", "x", 0.0, i);
        point.r = riderData->getAttribute("acc_data", "r", 0.0, i);
        // set the lowest startime & highest endtime
        if(point.tstamp<parsedDatapoints.lowestTimestamp) {
            parsedDatapoints.lowestTimestamp = point.tstamp;
        }
        if(point.tstamp>parsedDatapoints.highestTimestamp) {
            parsedDatapoints.highestTimestamp = point.tstamp;
        }
        parsedDatapoints.accelerometer.push_back(point);
    }
    
    ofLogVerbose("app::parseRiderData: XML Parsed");
    ofLogVerbose() << "app::parseRiderData:" << "Timestamp lowest" << parsedDatapoints.lowestTimestamp << "highest" << parsedDatapoints.highestTimestamp << "total time" << parsedDatapoints.highestTimestamp - parsedDatapoints.lowestTimestamp;
    ofLogVerbose() << "app::parseRiderData:" << "Longitude lowest" << parsedDatapoints.lowestLongitude << "highest" << parsedDatapoints.highestLongitude;
    ofLogVerbose() << "app::parseRiderData:" << "Latitude lowest" << parsedDatapoints.lowestLatitude << "highest" << parsedDatapoints.highestLatitude;
    ofLogVerbose() << "app::parseRiderData:" << "Altitude lowest" << parsedDatapoints.lowestAltitude << "highest" << parsedDatapoints.highestAltitude;
    
    if (parsedDatapoints.highestTimestamp <= parsedDatapoints.lowestTimestamp) {
        ofLogError("test: failed to set the start and end times correctly");
    }
    
    return parsedDatapoints;
}

//--------------------------------------------------------------
void app::keyPressed(int key){

}

//--------------------------------------------------------------
void app::keyReleased(int key){

}

//--------------------------------------------------------------
void app::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void app::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void app::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void app::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void app::windowResized(int w, int h){

}

//--------------------------------------------------------------
void app::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void app::dragEvent(ofDragInfo dragInfo){ 
    
    ofLogVerbose("drag event");

}