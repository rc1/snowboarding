#pragma once

#include "ofMain.h"
#include "ofxUI.h"
// #include "ofxGrabCam.h"

struct GPSDataPoint {
    double tstamp;
    double longitude;
    double latitude;
    double altitude;
    double ground_speed;
};

struct AccelerometerDataPoint {
    double tstamp;
    double x;
    double y;
    double z;
    double r;
};

struct HeartRateDataPoint {
    double tstamp;
    double averageOver32Beats;
    double averageOver16Beats;
    double averageOverLastBeats;
};

struct DataPoints {
    vector<GPSDataPoint> gps;
    vector<AccelerometerDataPoint> accelerometer;
    vector<HeartRateDataPoint> heartrate;
    
    // time min/max
    double lowestTimestamp;
    double highestTimestamp;
    
    // gps min/max
    double lowestLongitude;
    double lowestLatitude;
    double lowestAltitude;
    double lowestHeartrate;
    double lowestSpeed;
    double highestLongitude;
    double highestLatitude;
    double highestAltitude;
    double highestSpeed;
    double highestHeartrate;
};

class app : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
        // ofxGrabCam camera;
    
        ofxUICanvas *gui;
    
        void exit(); 
        void guiEvent(ofxUIEventArgs &e);
        
        ofVec4f heartrateSize;
        ofVec4f speedSize;
        ofVec4f altitudeSize;
    
    protected:
        ofVideoPlayer boarderVideo;
        float boarderAspectRatio;
    
        DataPoints parseRiderData(string filename);
        DataPoints datapoints;
        ofPoint latLongTL;
        ofPoint latLongBR;
    
        void drawBorderPath();
    
};
